#!/usr/bin/env perl
# build_release.pl - build an OpenBSD release
# originally written by Kyle Isom <kyle@tyrfingr.is>
# 
# ISC-licensed - see LICENSE.

use warnings;
use strict;
use Getopt::Std;
use File::Copy; 
use File::Temp qw/ tempfile tempdir /;

#############
# VARIABLES #
#############
my %opts = ( );         # hash to collect options in

my $arch = '';          # arch to build release on
my $release = '';       # stores OpenBSD release 
my $site = '';
my $man = 1;            # include the man pages
my $xbase = 0;          # include X11 sets
my $comp = 1;           # include compiler sets
my $games = 0;          # include game set
my $build = '';         # the build dir
my $mirror = "ftp://ftp.openbsd.org" ;
my $local_sets_path = '';
                        # path to the sets
my $retcode = 0;
my $buildplatform = `uname -s`;
my $build_sets = 0;     # whether to build siteXX from
                        # staging dir
my $sets_path = "";     # path to custom site files
my $iso = "";           # output file
my $fetch = 0;          # fetch files via ftp
my $no_custom = 0;      # do not include a custom site file
my $tempdir = '';       # temp dir to copy sets to
my $interactive = 0;    # do an interactive build
my $filelist = "";      # list of files needed
my @file_arr = ();      # array to hold files
my $relver = "";        # used to build filenames
my $version = "";
                        # the version number, populated by config.sh
chomp($buildplatform);


# get the current arch if on OpenBSD
if ('OpenBSD' eq $buildplatform) {
    # get current arch and release to use as the default
    $arch = `uname -m`;
    $release = `uname -r`; 
    chomp($arch);
    chomp($release);
}
else {
    # if not on OpenSBD, it's harder to pick a default
    # arch and release
    $arch = '';
    $release = '';
}


# options
#   -a <arch>   set architecture
#   -c          include compiler sets
#   -f <mirror> set the FTP mirror
#   -g          include gamesXX.tgz
#   -h          short help message
#   -m          include man pages
#   -n          do not fetch files
#   -o <path>   iso output directory         
#   -r <rel>    set release
#   -s <path>   build siteXX.tgz from <path>         
#   -s none     do not add in a site file
#   -v          use a vanilla build (no siteXX)
#   -V          print version information
#   -x          include X11 sets

getopts('a:cf:ghimno:r:s:vVx', \%opts);

# parse options and set relevant vars
while ( my ($key, $value) = each(%opts) ) {
    if ("a" eq $key) {
        $arch = $value;
    }

    if ("c" eq $key) {
        $comp = 1;
    }

    if ("f" eq $key) {
        $mirror = "ftp://$value";
    }

    if ("g" eq $key) {
        $games = 1;
    }

    if ("h" eq $key) {
        &help();
        exit;
    }

    if ("i" eq $key) {
        $interactive = 1;
    }

    if ("m" eq $key) {
        $man = 1;
    }

    if ("n" eq $key) {
        $fetch = 0;
    }

    if ("o" eq $key) {
        $iso = $value;
    }

    if ("r" eq $key) {
        $release = $value;
    }

    if ("s" eq $key) {
        $sets_path = $value;
    }

   if ("v" eq $key) {
        print "vanilla\n"; 
        $no_custom = 1;
    }

    if ("V" eq $key) {
        print "build_release version $version\n";
        exit;
    }

    if ("x" eq $key) {
        $xbase = 1;
    }
}

# can't build without a release or arch
if (!$interactive && (("" eq $release) || ("" eq $arch))) {
    die "[!] invalid arch $arch or release $release" ;
}
# check to make sure the tools needed are present
elsif (system("which mkisofs 2>&1 > /dev/null")) {
    die "[!] cdrtools doesn't appear to be installed";
}
elsif (system("which curl 2>&1 > /dev/null")) {
    die "[!] curl doesn't appear to be installed";
}
else {
    print "[+] building install iso for OpenBSD-$release/$arch\n";
}

$tempdir = tempdir(CLEANUP => 1);

print "using temporary directory: $tempdir\n";

if (! $interactive) {
    # set paths for build
    if (scalar @ARGV == 2) {
        $site = $ARGV[0];
        $build = $ARGV[1];
    }
    elsif ((scalar @ARGV == 1) and ($build_sets)) {
        $build = $ARGV[0];
    }

    elsif ((scalar @ARGV == 1) and ($no_custom)) {
        $build = $ARGV[0];
    }

    elsif (!$fetch) {
        my $msg = "need to specify ";
        if (! $no_custom) {
            $msg .= " the site root/tarball and ";
        }
        $msg .= "the path to the sets!";
        die $msg;
    }
} else {
    &interactive_mode();
}

$local_sets_path = "$build/$release/$arch";

$mirror = "$mirror/pub/OpenBSD/$release/$arch";
$relver = $release;
$relver =~ s/[.]//g;
$filelist = qq(INSTALL.$arch SHA256 base$relver.tgz bsd bsd.rd bsd.mp );
$filelist = $filelist . qq(cdboot cdbr etc$relver.tgz pxeboot);
@file_arr = split(/ /, $filelist);
if ($comp) {
    push(@file_arr, "comp$relver.tgz");
}

if ($man) {
    push(@file_arr, "man$relver.tgz");
}

if ($games) {
    push(@file_arr, "$games$relver.tgz");
}

if ($xbase) {
    push(@file_arr, "xbase$relver.tgz");
    push(@file_arr, "xetc$relver.tgz");
    push(@file_arr, "xfont$relver.tgz");
    push(@file_arr, "xserv$relver.tgz");
    push(@file_arr, "xshare$relver.tgz");
}


if (!$iso) {
    $iso = "$build/release$release.iso";
    $iso =~ s/[.]// ;
}

if (! -d $local_sets_path) {
    $retcode = system("mkdir -p $local_sets_path");
    if ($retcode != 0) {
        die "could not create $local_sets_path" ;
    }
}

if ((! $build_sets) and (! $no_custom)) {
    my $matchsite = "site$release" ;
    $matchsite =~ s/[.]//;
    $matchsite = "$matchsite.tgz";
    if (!($site =~ /^[\/.\w\s]*$matchsite/)) {
        die "invalid site file $site";
    }
    else {
        $retcode = system("cp $site $local_sets_path");
        if ($retcode) {
            die "could not copy $site to $local_sets_path";
        }
    }
}
elsif (! $no_custom) {
    $site = "site$release";
    $site =~ s/[.]// ;
    $site = $site . '.tgz';

    if (-d $sets_path) {
        if (!chdir($sets_path)) {
            die "could not chdir to $sets_path";
        }    
        $retcode = system("tar czf $local_sets_path/$site *");
        if ($retcode) {
            die "tarfile failed";
        }
    }
    else {
        die "invalid local sets path $sets_path";
    }
}

if (-e -z $site) {
    die "empty / invalid $site";
}

for (@file_arr) {
    print "$local_sets_path/$_ -> $tempdir/$_\n";
    copy("$local_sets_path/$_", "$tempdir/$_")      or 
        die "could not copy $_ from $local_sets_path to $tempdir!";
}

$local_sets_path = $tempdir;
chdir($local_sets_path) or die "could not chdir to $local_sets_path";

if ($fetch) {
    print "fetching sets...\n";
    foreach (@file_arr) {
        print "\tfetching $_\n";
        $retcode = system("curl -C - --ftp-pasv $mirror/$_ -o $_");
        if (0 != $retcode) {
            die "could not fetch $_ from $mirror!";
        }
    }
}

if (!$man and -s "man$relver.tgz") {
    if (!unlink("man$relver.tgz")) { 
        die "could not remove man page set: $!";
    }
}

if (!$comp and -s "comp$relver.tgz") {
    if (!unlink("comp$relver.tgz")) {
        die "could not remove compiler set: $!";
    }
}

$retcode = system("ls x* 2> /dev/null");
if (!$xbase and !$retcode) {
    if (system("rm x*")) {
        die "could not remove X11 sets";
    }
}

if (!$games and -e -s "games$relver.tgz") {
    if (!unlink("games$relver.tgz")) {
        die "could not remove game set";
    }
}

# remove floppy boot images
print "local sets path: $local_sets_path\n";
$retcode = system("ls $local_sets_path/floppy*");
if (!$retcode and (!unlink glob "floppy*")) {
    die "could not remove floppy boot images!"
}

if (! (chdir $build)) {
    die "could not chdir to build root!";
}

my $mkisofs = " mkisofs -r -no-emul-boot -b $release/$arch/cdbr ";
$mkisofs = "$mkisofs -c boot.catalog -o $iso $build";

$retcode = system($mkisofs);
if ($retcode) {
    die "mkisofs failed";
}
else {
    print "\n\n\ncreated $iso\n";
}

exit;

# stub for future interactive mode
sub interactive_mode {
    my $answer = '';

    $arch = &prompt("arch", $arch, 0);
    $release = &prompt("release", $release, 0);
    $comp = &prompt("include compiler set", $comp, 1);
    $man = &prompt("include man pages", $man, 1);
    $xbase = &prompt("include X11 sets", $xbase, 1);
    $games = &prompt("include games", $games, 1);
    $fetch = &prompt("fetch sets", $fetch, 1);

    if ($fetch == 1) {
        $mirror = &prompt("mirror", $mirror, 0);
    }

    $no_custom = ! &prompt("include custom site set", $no_custom, 1);
    
    if ($no_custom == 0) {
        $sets_path = &prompt("path to custom set or custom root", 
                             $sets_path, 0);

    } else {
        $sets_path = '';
    }

    $iso = &prompt("iso file to generate", $iso, 0);
    $build = &prompt("build directory", $build, 0);
    $build =~ s/\/$//;
    print "build: $build\n";
    return;
}

sub prompt {
    my ($msg, $default, $numeric) = @_;
    my $answer = '';

    print "$msg";
    if ($numeric && "$default" ne "") {
        if ($default == 1) {
            print " [yes]";
        } elsif ($default == 0) {
            print " [no]";
        }
    } else {
        if ($default ne '') {
            print " [$default]";
        }
    }
    print ": ";

    chomp($answer = <STDIN>);
    if ($numeric == 0) {
        if ($answer eq '') {
            $answer = $default;
        }

        $answer =~ s/^~\//$ENV{'HOME'}\//;
    } else {
        if ($answer =~ /^y(es)?$/) {
            $answer = 1;
        } elsif ($answer =~ /^no?$/) {
            $answer = 0;
        } elsif ($answer eq '') {
            $answer = $default;
        } else {
            $answer = &prompt($msg, $default, $numeric);
        }
        
    }

    return $answer;
}

sub help {
    print <<EOF;
build_release version $version
build a custom OpenBSD release iso
author: Kyle Isom <kyle\@tyrfingr.is>

usage: build_release.pl [options] [site] build_dir]

 options
   -a <arch>    set architecture
   -c           include compiler sets
   -f <mirror>  set the FTP mirror
   -g           include gamesXX.tgz
   -h           short help message
   -m           include man pages
   -n           do not fetch files
   -o <path>    iso output file
   -r <rel>     set release
   -s <path>    build siteXX.tgz from <path>         
   -s none      do not add in a site file
   -v           use a vanilla build (no siteXX)
   -V           print version information
   -x           include X11 sets
EOF
}
